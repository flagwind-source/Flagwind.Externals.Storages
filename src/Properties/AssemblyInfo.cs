﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Flagwind.Externals.Storages")]
[assembly: AssemblyDescription("This is a library about storages.")]
[assembly: AssemblyCompany("Flagwind Corporation")]
[assembly: AssemblyProduct("Flagwind.Externals.Storages Library")]
[assembly: AssemblyCopyright("Copyright(C) Flagwind Corporation 2017. All rights reserved.")]

[assembly: ComVisible(false)]
[assembly: Guid("608456ca-4f27-43a5-abe2-1cc204c25a81")]

[assembly: AssemblyVersion("1.0.1.311")]
[assembly: AssemblyFileVersion("1.0.1.311")]
