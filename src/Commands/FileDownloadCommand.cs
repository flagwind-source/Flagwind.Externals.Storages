﻿using System;
using System.IO;
using System.ComponentModel;

using Flagwind.Resources;
using Flagwind.Services;

namespace Flagwind.Externals.Storages.Commands
{
	[DisplayName("${Text.Storages.FileDownloadCommand.Title}")]
	[Description("${Text.Storages.FileDownloadCommand.Description}")]
	public class FileDownloadCommand : CommandBase<CommandContext>
	{
		#region 成员字段

		private StorageClient _client;

		#endregion

		#region 公共属性

		public StorageClient Client
		{
			get
			{
				return _client;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException();

				_client = value;
			}
		}

		#endregion

		#region 构造方法

		public FileDownloadCommand() : base("Download")
		{

		}

		#endregion

		#region 重写方法

		protected override object OnExecute(CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new CommandException(ResourceUtility.GetString("${Text.MissingCommandArguments}"));

			// 获取要删除的文件路径
			var path = context.Expression.Arguments[0];

			// 调用客户端进行下载操作
			var result = TaskHelper.ExecuteTask(() => this.Client.Download(path));

			return result;
		}

		#endregion
	}
}