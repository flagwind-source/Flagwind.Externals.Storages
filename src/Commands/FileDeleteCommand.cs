﻿using System;
using System.ComponentModel;

using Flagwind.Resources;
using Flagwind.Services;

namespace Flagwind.Externals.Storages.Commands
{
	[DisplayName("${Text.Storages.FileDeleteCommand.Title}")]
	[Description("${Text.Storages.FileDeleteCommand.Description}")]
	public class FileDeleteCommand : CommandBase<CommandContext>
	{
		#region 成员字段

		private StorageClient _client;

		#endregion

		#region 公共属性

		public StorageClient Client
		{
			get
			{
				return _client;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException();

				_client = value;
			}
		}

		#endregion

		#region 构造方法

		public FileDeleteCommand() : base("Delete")
		{

		}

		#endregion

		#region 重写方法

		protected override object OnExecute(CommandContext context)
		{
			if(context.Expression.Arguments.Length < 1)
				throw new CommandException(ResourceUtility.GetString("${Text.MissingCommandArguments}"));

			// 获取要删除的文件路径
			var path = context.Expression.Arguments[0];

			// 调用客户端进行删除操作
			var result = TaskHelper.ExecuteTask(() => this.Client.Delete(path));

			return result;
		}

		#endregion
	}
}
