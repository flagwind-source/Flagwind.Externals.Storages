﻿using System;
using System.Collections.Generic;

using Flagwind.IO;
using Flagwind.Externals.Storages.Configuration;

namespace Flagwind.Externals.Storages
{
	[Flagwind.Services.Matcher(typeof(FileSystem.Matcher))]
	public class RemoteFileSystem : IFileSystem
	{
		#region 单例字段

		public static readonly RemoteFileSystem Instance = new RemoteFileSystem();

		#endregion

		#region 成员字段

		private GenericOption _option;

		#endregion

		#region 公共属性

		/// <summary>
		/// 获取Web文件目录系统的方案，始终返回“Flagwind.remote”。
		/// </summary>
		public string Scheme
		{
			get
			{
				return "Flagwind.remote";
			}
		}

		public IFile File
		{
			get
			{
				return LocalFileSystem.Instance.File;
			}
		}

		public IDirectory Directory
		{
			get
			{
				return LocalFileSystem.Instance.Directory;
			}
		}

		public GenericOption Option
		{
			get
			{
				return _option;
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException();

				_option = value;
			}
		}

		#endregion

		#region 构造方法

		public RemoteFileSystem()
		{

		}

		#endregion

		#region 公共方法

		public string GetUrl(string path)
		{
			var url = string.Format("{0}?path={1}", this.Option.FileApi, path);

			return url;
		}

		#endregion
	}
}
