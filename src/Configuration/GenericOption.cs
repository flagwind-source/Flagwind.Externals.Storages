﻿using System;
using System.Collections.Generic;

using Flagwind.Options.Configuration;

namespace Flagwind.Externals.Storages.Configuration
{
	public class GenericOption : OptionConfigurationElement
	{
		#region 常量定义

		public const string XML_FILE_API = "fileApi";

		#endregion

		#region 公共属性

		/// <summary>
		/// 获取或设置文件访问的接口地址。
		/// </summary>
		[OptionConfigurationProperty(XML_FILE_API, OptionConfigurationPropertyBehavior.IsRequired)]
		public string FileApi
		{
			get
			{
				return (string)this[XML_FILE_API];
			}
			set
			{
				this[XML_FILE_API] = value;
			}
		}

		#endregion
	}
}
