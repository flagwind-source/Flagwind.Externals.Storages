﻿using System;
using System.Collections.Generic;

namespace Flagwind.Externals.Storages
{
	public class FileUploadResult
	{
		#region 公共属性

		/// <summary>
		/// 获取或设置文件名。
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置文件路径。
		/// </summary>
		public PathInfo Path
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置文件访问URL。
		/// </summary>
		public string Url
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置创建时间。
		/// </summary>
		public DateTime CreatedTime
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置修改时间。
		/// </summary>
		public DateTime ModifiedTime
		{
			get;
			set;
		}

		#endregion

		#region 嵌套子类

		public class PathInfo
		{
			public string Scheme
			{
				get;
				set;
			}

			public string DirectoryName
			{
				get;
				set;
			}

			public string FileName
			{
				get;
				set;
			}

			public string FullPath
			{
				get;
				set;
			}

			public string Url
			{
				get;
				set;
			}

			public bool IsFile
			{
				get;
				set;
			}

			public bool IsDirectory
			{
				get;
				set;
			}
		}

		#endregion
	}
}
